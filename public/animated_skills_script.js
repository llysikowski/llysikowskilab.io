let skillsFilled = false;

let fillProgress = () => {
  if (
    !skillsFilled &&
    $(window).scrollTop() + $(window).height() > $(".skills").offset().top
  ) {
    skillsFilled = true;
    console.log("Fired!");
    $(".progress-bar").each(function () {
      $(this).animate(
        {
          width: $(this).attr("data-percent"),
        },
        1000
      );
    });
  }
};

$(window).scroll(fillProgress);
fillProgress();
